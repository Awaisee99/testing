<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// To show All records of User and Daily Attendance (It will show record of all users or attendances)
Route::get('/admin/attendance/all','AttendanceController@all');
Route::get('/admin/user/all','UserController@all');


//To search a specific one
//(It will show specific record against any date:(e.g.)/admin/attendance/search?date=10-10-18)
Route::get('/admin/attendance/search','AttendanceController@search');
//(It will show specific record against any User Id or name:(e.g.)/admin/user/search?id=1 or ?name=awais)
Route::get('/admin/user/search','UserController@search');


//To delete
//(It will delete a specific record against any date:(e.g.)/admin/attendance/delete?date=10-10-18)
Route::get('/admin/attendance/delete','AttendanceController@delete');
//(It will delete specific record against any User Id or name:(e.g.)/admin/user/delete?id=1 or ?name=awais)
Route::get('/admin/user/delete','UserController@delete');
