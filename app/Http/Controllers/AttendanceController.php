<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Attendance;
use Illuminate\Support\Facades\Input;


class AttendanceController extends Controller
{
    public function all()
    {
        $query = DB::select('select * from attendances');
        return $query;
    }

    public function search()
    {
        $date = Input::get('date');
        if (empty($date))
            return "Record not Found";
        else {
            $query = DB::select('select * from attendances where date = ?', [$date]);
            if (empty($query))
                return "Record not Found";
            else
                return $query;
        }
    }

    public function delete()
    {
        $date = Input::get('date');
        if (empty($date))
            return "Record not Found";
        else {
            $query = DB::delete('delete from attendances where date = ?', [$date]);
            if(empty($query))
                return "No Match";
            else {
                $q = DB::select('select * from attendances');
                return $q;
            }
        }
    }
}
